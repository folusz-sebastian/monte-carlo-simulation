package zad2;

public class ServiceStation {
    private final double servicingTime;
    private double timeToWhenItsBusy;

    public ServiceStation(double servicingTime) {
        this.servicingTime = servicingTime;
        this.timeToWhenItsBusy = 0;
    }

    public void startServicing(double simTime) {
        this.timeToWhenItsBusy = simTime + servicingTime;
    }

    public double getTimeToWhenItsBusy() {
        return timeToWhenItsBusy;
    }
}
