package zad2;

import randomGenerator.SimGenerator;
import java.util.List;
import java.util.Optional;

public class Simulation {
    private double currentTime;
    private final double maxSimTime;
    private final List<ServiceStation> serviceStations;
    private int numberOfRealizedServices;
    private final int maxNumberOfRealizedServices;
    private int numberOFUnservedServices;
    private final double expectedValueOfDistribution;

    public Simulation(List<ServiceStation> serviceStations, int maxNumberOfRealizedServices, double maxSimTime, double expectedValueOfDistribution) {
        this.serviceStations = serviceStations;
        this.maxNumberOfRealizedServices = maxNumberOfRealizedServices;
        this.maxSimTime = maxSimTime;
        this.expectedValueOfDistribution = expectedValueOfDistribution;
        this.currentTime = 0;
        this.numberOfRealizedServices = 0;
        this.numberOFUnservedServices = 0;
    }

    public void runSimulation() {
        while (numberOfRealizedServices < maxNumberOfRealizedServices && currentTime < maxSimTime) {
            Optional<ServiceStation> firstAvailableService =
                    serviceStations.stream().filter(station -> currentTime >= station.getTimeToWhenItsBusy()).findFirst();
            if (firstAvailableService.isPresent()) {
                ServiceStation station = firstAvailableService.get();
                station.startServicing(currentTime);
                numberOfRealizedServices ++;
                logServicing(station);
            } else {
                System.out.println("Lines are busy in time: [" + currentTime + "]");
                numberOFUnservedServices++;
            }
            currentTime += generateTimeOfNextServicing();
        }
        if (currentTime > maxSimTime) {
            System.out.println("the simulation time has been exceeded");
        }
    }

    private void logServicing(ServiceStation station) {
        System.out.println("service id: " + station);
        System.out.println("servicing from time: [" + currentTime + "] to: [" + station.getTimeToWhenItsBusy() + "]");
        System.out.println();
    }

    private double generateTimeOfNextServicing() {
        SimGenerator simGenerator = new SimGenerator();
        return simGenerator.exponential(expectedValueOfDistribution);
    }

    public int getNumberOFUnservedServices() {
        return numberOFUnservedServices;
    }

    public int getNumberOfRealizedServices() {
        return numberOfRealizedServices;
    }
}
