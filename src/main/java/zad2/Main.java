package zad2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    private static final int MAX_NUMBER_OF_SERVICES = 7;
    private static final double MAX_SIMULATION_TIME = 30.0;
    private static final double EXPECTED_VALUE_OF_THE_DISTRIBUTION = 5.0;
    public static void main(String[] args) {
        ServiceStation s1 = new ServiceStation(5.0);
        ServiceStation s2 = new ServiceStation(3.0);
        List<ServiceStation> serviceStations = new ArrayList<>(Arrays.asList(s1,s2));

        Simulation simulation = new Simulation(
                serviceStations,
                MAX_NUMBER_OF_SERVICES,
                MAX_SIMULATION_TIME,
                EXPECTED_VALUE_OF_THE_DISTRIBUTION
        );
        simulation.runSimulation();

        System.out.println("NumberOFUnservedServices: " + simulation.getNumberOFUnservedServices());
        System.out.println("NumberOfRealizedServices: " + simulation.getNumberOfRealizedServices());
    }
}
