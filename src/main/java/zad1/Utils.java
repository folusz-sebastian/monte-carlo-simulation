package zad1;

public class Utils {
    static double countError(double exactValue, double measuredValue) {
        return Math.abs(exactValue - measuredValue);
    }
}
