package zad1;

public class Main {
    private static final double xp = 1;
    private static final double xk = Math.E;

    public static void main(String[] args) {
        DefiniteIntegral integral = new DefiniteIntegral(xp, xk) {
            public double function(double x) {
                return 3 / x;
            }
        };

        for (int i = 0; i < 10; i++) {
            MonteCarlo monteCarlo = new MonteCarlo(integral);
            double result = monteCarlo.countIntegral();
            System.out.println("wynik całkowania metodą Monte Carlo: " + result);
            System.out.println("wylosowana licza punktów: " + monteCarlo.getNumberOfSections());
            System.out.println("błąd bezwględny: " + Utils.countError(3, result));
        }
    }
}
