package zad1;

public abstract class DefiniteIntegral {
    private final double startBound;
    private final double endBound;

    public DefiniteIntegral(double startBound, double endBound) {
        this.startBound = startBound;
        this.endBound = endBound;
    }

    public abstract double function(double x);

    public double getStartBound() {
        return startBound;
    }

    public double getEndBound() {
        return endBound;
    }
}
