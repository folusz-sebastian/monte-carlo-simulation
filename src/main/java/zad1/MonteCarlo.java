package zad1;

import randomGenerator.SimGenerator;

public class MonteCarlo {
    private final DefiniteIntegral integral;
    private final int numberOfSections;
    private final double singleSectionLength;
    private static final double upperNumberOfDrawing = 50;

    public MonteCarlo(DefiniteIntegral integral) {
        this.integral = integral;
        this.numberOfSections = generateNumberOfSections();
        this.singleSectionLength = (integral.getEndBound() - integral.getStartBound()) / numberOfSections;
    }

    public double countIntegral() {
        double sectionAreasSum = sumSectionAreas();
        return this.singleSectionLength * sectionAreasSum;
    }

    private int generateNumberOfSections() {
        SimGenerator simGenerator = new SimGenerator();
        return (int) simGenerator.uniform(1, upperNumberOfDrawing);
    }

    private double sumSectionAreas() {
        double sum = 0;
        for (int i = 1; i <= numberOfSections; i++) {
            double functionArgument = integral.getStartBound() + i * singleSectionLength;
            sum += integral.function(functionArgument);
        }
        return sum;
    }

    public int getNumberOfSections() {
        return numberOfSections;
    }
}
