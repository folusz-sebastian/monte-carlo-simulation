# Przykład symulacji Monte Carlo

##  Opis projektu
W skład projektu wchodzą dwa zadania przedstawiające przykład wykorzystania symulacji Monte Carlo.

Zad. 1
Oszacować wartość całki metodą Monte Carlo poprzez:

- losowy wybór (z rozkładem równomiernym) n punktów x_i w przedziale <a,b>
- wyznaczenie i zsumowanie pól prostokątów opartych na odcinkach wyznaczonych przez wylosowane punkty

Zbadać dokładność/błąd oszacowania zależnie od liczby punktów n.

Zad. 2
System składa się z N linii (stanowisk obsługi), z których każda może obsługiwać klientów. Oszacować ile zgłoszeń obsłuży system, a ile razy odrzuci zgłoszenia

Zgłoszenia pojawiają się w losowych chwilach czasu:

- każde zgłoszenie trafia najpierw na linię nr 1; jeśli w chwili t_k pojawienia się k-tego zgłoszenia linia jest wolna, to zgłoszenie jest obsługiwane w ciągu czasu Tz_i minut (czas zajętości i-tej linii, czyli obsługi - jest wartością stałą zadawaną osobno dla każdej linii);
- jeśli natomiast w momencie t_k linia jest zajęta, wówczas zgłoszenie przekazywane jest na kolejną linię itd.;
- jeśli wszystkie linie w chwili t_k są zajęte, układ odmawia obsługi zgłoszenia (odrzuca je); 
- Strumień zgłoszeń do systemu ma rozkład Poissona (odstępy czasu między kolejnymi zgłoszeniami są zmienną losową o rozkładzie wykładniczym);